#!/usr/bin/env node
import C from 'chalk';
import fs from 'fs';
import path from 'path';
import Debug from 'debug';
import webpack from 'webpack';
import express from 'express';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import {
  checkSumFromString,
  fileExists,
  reconfigureOmniWebWebpackConfiguration,
  resolveClosestPackageJson,
  webpackBuildGlobalOmniWebCss,
} from './helpers';
import * as ChromeLauncher from 'chrome-launcher';
import getPort from 'get-port';
import { Flag, JumpstartTarget } from './types';
import { createComponentStructure, createFakeAppEntryPoint, createJumpstartFile } from './creators';

const debug = Debug('jumpstart');
const [, , targetFile, ...flags] = process.argv;
const flagDescription = {
  [Flag.create]: ['Create the actual component'],
  [Flag.noLaunch]: ['Do not open in Chrome'],
  [Flag.noJumpstartFile]: ['Skip creation of jumpstart-file (e.g. MyComponent.jumpstart.jsx'],
  [Flag.temp]: ['Remove jumpstart-file when closing'],
  [Flag.help]: ['This message'],
};
const showHelp = () => {
  console.log(C.bold('jumpstart MyComponent.jsx[ --some-flag]'));
  console.log(C.gray('└─ .jsx, .js, .ts or .tsx file supported\n'));
  console.log(C.cyan(`Have Chrome Canary, but want to open in Chrome?`));
  console.log(C.cyan.dim('└─ BASH: export CHROME_PATH=/Applications/Google Chrome.app/Contents/MacOS/Google Chrome'));
  console.log(C.cyan.dim('└─ FISH: set -x CHROME_PATH /Applications/Google Chrome.app/Contents/MacOS/Google Chrome\n'));
  Object.entries(flagDescription).map(([flag, description]) => console.log(`${C.bold(flag)}: ${description}`));
  process.exit(0);
};
const isOn = (f: Flag) => flags.includes(f);
flags.forEach((flag) => {
  if (!(flag in flagDescription)) {
    console.log(C.red.bold(`unknown flag "${flag}"\n`));
    showHelp();
    process.exit(1);
  }
});
if (isOn(Flag.help)) {
  showHelp();
}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Validate input
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if (!targetFile || !targetFile.match(/\.(jsx?|tsx?)$/)) {
  console.error(C.red.bold(!targetFile ? 'No input file given' : `Invalid file type given\n`));
  showHelp();
  process.exit(1);
}

if (!fileExists(targetFile) && !isOn(Flag.create)) {
  console.error(C.cyan.bold(`The component "${targetFile}" did not exist`));
  console.error(C.cyan.dim(`💡 you can create it by using the ${C.bold('--create')} option`));
  process.exit(1);
}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// PATHS AND FILES RESOLVING
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const workingDir = process.cwd();
const packageJsonRoot = resolveClosestPackageJson(workingDir, ({ name }) => name === 'omniweb');
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const jumpstartNodeModulesDir = path.resolve(__dirname, '../', 'node_modules');
const jumpstartDir = path.resolve(__dirname);
const buildDir = path.join(__dirname, '../', '.build');

const target: JumpstartTarget = {
  relativeFilePath: targetFile,
  relativePath: path.dirname(targetFile),
  filePath: path.resolve(workingDir, targetFile),
  path: path.dirname(path.resolve(workingDir, targetFile)),
  fileName: path.basename(targetFile),
  name: path.basename(targetFile, path.extname(targetFile)),
  extension: path.extname(targetFile),
  componentName: '',
  jumpstartFileName: '',
  jumpstartFilePath: '',
  styleFilePath: '',
  testFileName: '',
  testFilePath: '',
  checkSum: '',
  buildDir: '',
  globalCssFileName: '',
  globalCssFile: '',
  dummyAppFile: '',
  envSetupFile: '',
  mountInAppTarget: {
    componentName: '',
    filePath: '',
  },
};
target.componentName = target.name
  .split('.')
  .map((p) => `${p[0].toUpperCase()}${p.slice(1)}`)
  .join('');
target.jumpstartFileName = `${target.name}.jumpstart${target.extension}`;
target.jumpstartFilePath = path.resolve(target.path, target.jumpstartFileName);
target.styleFilePath = path.resolve(target.relativePath, 'style.scss');
target.testFileName = `${target.name}.test${target.extension}`;
target.testFilePath = path.resolve(target.path, target.testFileName);
target.checkSum = checkSumFromString(target.filePath);
target.buildDir = path.join(buildDir, target.checkSum);
target.globalCssFileName = 'main.css';
target.globalCssFile = path.join(buildDir, target.globalCssFileName);
target.dummyAppFile = path.join(target.buildDir, 'App.jsx');
target.envSetupFile = path.join(target.buildDir, 'setup.prod.js');

target.mountInAppTarget = {
  componentName: target.componentName,
  filePath: target.filePath,
};

const webpackClientConfig = path.resolve(packageJsonRoot.moduleDir, 'packages', 'app-web', 'webpack.dev.client.js');

// Create the component?
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if (isOn(Flag.create)) {
  if (!fileExists(target.jumpstartFilePath)) {
    createComponentStructure(target, debug);
  } else {
    debug(`${Flag.create} was used, but the component already existed, skipping step`);
  }
}

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Should we use a jumpstart-file?
// ////////////////////////////////styleFilePath/////////////////////////////////////////////////////////////////////////////////////
const jumpstartFileDidAlreadyExists = fileExists(target.jumpstartFilePath);

if (!isOn(Flag.noJumpstartFile) && !targetFile.includes('.jumpstart.')) {
  debug('target file "%s" was NOT a jumpstart-file', targetFile);
  // create jumpstart-file if it doesn't exist
  if (!jumpstartFileDidAlreadyExists) {
    createJumpstartFile(target, isOn(Flag.create), debug);
  } else {
    debug('jumpstart-file (%s) already exists', target.jumpstartFileName, targetFile);
  }
  // mount the jumpstart file in the dummy-app instead of the component
  target.mountInAppTarget.componentName = `${target.mountInAppTarget.componentName}Jumpstart`;
  target.mountInAppTarget.filePath = target.jumpstartFilePath;
  debug('mount jumpstart-file in fake app %o', target.mountInAppTarget);
} else {
  debug('target file "%s" was a jumpstart-file, using it directly...', targetFile);
}

if (isOn(Flag.temp)) {
  if (!jumpstartFileDidAlreadyExists) {
    debug(`${Flag.temp} flag raised, defering cleanup on exit`);
    process.stdin.resume();
    let isAlreadyCleand = false;
    function onProgramExit() {
      if (isAlreadyCleand) return;
      debug(`program is exiting and the ${Flag.temp} flag raised, cleaning now`);
      debug(`removing ${target.jumpstartFilePath}`);
      try {
        fs.unlinkSync(target.jumpstartFilePath);
        isAlreadyCleand = true;
      } catch (e) {
        console.error(e);
      }
      process.exit();
    }

    //do something when app is closing
    process.on('exit', onProgramExit);

    //catches ctrl+c event
    process.on('SIGINT', onProgramExit);

    // catches "kill pid" (for example: nodemon restart)
    process.on('SIGUSR1', onProgramExit);
    process.on('SIGUSR2', onProgramExit);

    //catches uncaught exceptions
    process.on('uncaughtException', onProgramExit);
  } else {
    debug(`${Flag.temp} flag raised, but the jumpstart-file already existed (will not clean)`);
  }
}

// PREPARE THE ENTRY POINT
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
createFakeAppEntryPoint(target, debug);
fs.copyFileSync(path.resolve(jumpstartDir, './setup.prod.js'), target.envSetupFile);

// WEBPACK IT!
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const webpackConfig = reconfigureOmniWebWebpackConfiguration(
  target,
  webpackClientConfig,
  jumpstartNodeModulesDir,
  workingDir,
  debug
);

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// COMMON STYLE
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if (!fileExists(target.globalCssFile)) {
  webpackBuildGlobalOmniWebCss(target, packageJsonRoot.moduleDir, debug);
} else {
  debug('already have a global css-file from nt-css-rejumpstart (%s)', target.globalCssFile);
}

// Webpack needs to resolve .babel stuff from actual file location
process.chdir(workingDir);

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SERVE IT!
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
(async () => {
  const port = await getPort({ port: [...Array(20)].map((_, i) => 3005 + i) });
  const startingUrl = `http://localhost:${port}`;
  console.log(C.cyan(`✓ Preparing serving of ${target.fileName}`));
  console.log(C.cyan.dim(`  .. make global CSS available under "/build"`));
  console.log(C.cyan.dim(`  .. make OmniWeb-assets available under "/ow/assets"`));
  console.log('');
  if (!isOn(Flag.noLaunch)) {
    console.log(C.cyan.bold(`✓ Auto-launching Chrome when ready (${startingUrl})`));
  } else {
    console.log(C.cyan.bold(`✓ Open your browser at ${startingUrl}`));
  }
  console.log(C.cyan.dim(`  └─ ${target.filePath}`));
  if (!isOn(Flag.noJumpstartFile)) {
    console.log(C.cyan.dim(`  └─ ${target.jumpstartFilePath}`));
  }
  console.log(C.cyan.dim(`  └─ ${target.testFilePath}`));
  console.log(C.cyan.dim(`  └─ ${target.styleFilePath}`));
  console.log(C.cyan.dim(`  └─ ${target.dummyAppFile}`));

  const app = express();
  const compiler = webpack(webpackConfig);
  // Tell express to use the webpack-dev-middleware and use the webpack.config.js
  // configuration file as a base.
  app.use(
    webpackDevMiddleware(compiler, {
      publicPath: webpackConfig.output.publicPath,
      stats: 'errors-only',
      logLevel: 'silent',
    })
  );
  app.use('/assets', express.static(path.resolve(__dirname, 'static')));
  app.use('/build', express.static(buildDir));
  app.use(
    '/ow/assets',
    express.static(path.resolve(packageJsonRoot.moduleDir, 'packages/app-web/src/public/ow/assets'))
  );

  app.use(
    webpackHotMiddleware(compiler, {
      log: false,
      path: '/__webpack_hmr',
      heartbeat: 2000,
    })
  );
  webpackConfig.devServer.port = port;

  app.listen(port, function () {
    const chromeFlags = [
      '--auto-open-devtools-for-tabs',
      ...ChromeLauncher.Launcher.defaultFlags().filter(
        (flag) => !['--disable-extensions', '--disable-extensions'].includes(flag)
      ),
    ];
    if (!isOn(Flag.noLaunch)) {
      ChromeLauncher.launch({
        startingUrl,
        userDataDir: false,
        ignoreDefaultFlags: true,
        chromeFlags,
      }).catch((err) => {
        debug('Got error from Chrome launcher', err);
      });
    }
  });
})();

// [ ] create Component (folder, Component, test and jumpstart-file)
// [ ] run test?
// [ ] doc → debugger;
// [ ] start with prod config
// [ ] Env.setup() → make it easy to edit though
//     [ ] dynamically use Env from current OW-installation
//     [ ] make it editable vs --env-prod --env-dev
// [ ] Persist API-snapshot, use in tests??
