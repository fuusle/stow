import path from 'path';

export interface JumpstartTarget {
  relativeFilePath: string;
  relativePath: string;
  filePath: string;
  path: string;
  fileName: string;
  name: string;
  extension: string;
  checkSum: string;
  componentName: string;
  jumpstartFileName: string;
  jumpstartFilePath: string;
  styleFilePath: string;
  testFileName: string;
  testFilePath: string;
  buildDir: string;
  globalCssFileName: string;
  globalCssFile: string;
  dummyAppFile: string;
  envSetupFile: string;
  mountInAppTarget: {
    componentName: string;
    filePath: string;
  };
}

export enum Flag {
  create = '--create',
  noLaunch = '--no-launch',
  noJumpstartFile = '--no-jumpstart-file',
  temp = '--temp',
  help = '--help',
}
