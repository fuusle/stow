import fs from 'fs';
import path from 'path';
import crypto from 'crypto';
import { Debugger } from 'debug';
import getPort from 'get-port';
import C from 'chalk';
import express from 'express';
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import * as ChromeLauncher from 'chrome-launcher';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import { JumpstartTarget } from './types';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import ReactRefreshWebpackPlugin from '@pmmmwh/react-refresh-webpack-plugin';
import WebpackBar from 'webpackbar';

export function fileExists(file: string): boolean {
  return fs.existsSync(file);
}

export function dirExists(path: string): boolean {
  return fs.existsSync(path);
}

export function resolveClosestPackageJson(
  cwd: string,
  peek?: (content: { name: string }) => boolean
): { moduleDir: string; relativePathToTarget: string } {
  const pathLevels = cwd.split(path.sep);
  const peekHelper = (file: string): boolean => {
    const content = JSON.parse(fs.readFileSync(file, 'utf-8'));
    return !peek || peek(content);
  };
  for (let m = pathLevels.length; m > 0; m--) {
    const pathToLookIn = pathLevels.slice(0, m).join(path.sep);
    const packageJson = path.resolve(pathToLookIn, 'package.json');
    if (packageJson && fileExists(packageJson) && (!peek || peekHelper(packageJson))) {
      return { moduleDir: pathToLookIn, relativePathToTarget: pathLevels.slice(m).join(path.sep) };
    }
  }

  throw Error('Unable to find package.json');
}

export function checkSumFromString(str: string): string {
  return crypto.createHash('sha256').update(str, 'utf8').digest('hex');
}

export function webpackBuildGlobalOmniWebCss(target: JumpstartTarget, omniwebRootPath: string, debug: Debugger) {
  console.log(C.cyan.bold(`✓ Building CSS reboot/reset from nt-css-reboot`));
  webpack(
    {
      entry: path.resolve(omniwebRootPath, 'packages/nt-css-reboot/src/styles/main.scss'),
      output: {
        path: path.resolve(__dirname, '../.build'),
      },
      module: {
        rules: [
          {
            exclude: /node_modules/,
            test: /\.scss$/,
            use: [
              MiniCssExtractPlugin.loader,
              {
                loader: 'css-loader',
              },
              {
                loader: 'postcss-loader',
              },
              {
                loader: 'sass-loader',
              },
            ],
          },
        ],
      },
      plugins: [
        new MiniCssExtractPlugin({
          filename: target.globalCssFileName,
        }),
      ],
    },
    (err, stats) => {
      // Stats Object
      if (err || stats.hasErrors()) {
        console.error('Error found generating global css');
        console.error(err);
        return;
      }
      console.log(C.cyan.dim(`  .. done!`));
      debug('successfully generated global css from OmniWeb/nt-css-rejumpstart (%s)', target.globalCssFile);
    }
  );
}

export function reconfigureOmniWebWebpackConfiguration(
  target: JumpstartTarget,
  webpackClientConfig: string,
  jumpstartNodeModulesDir: string,
  workingDir,
  debug: Debugger
) {
  const owWebpackConfig = require(webpackClientConfig);
  const overrides = {
    ...owWebpackConfig,
    context: workingDir,
    entry: ['webpack-hot-middleware/client', target.dummyAppFile],
    resolve: {
      ...owWebpackConfig.resolve,
      alias: {
        'react-refresh': dirExists(path.resolve(jumpstartNodeModulesDir, 'react-refresh'))
          ? path.resolve(jumpstartNodeModulesDir, 'react-refresh')
          : path.resolve(jumpstartNodeModulesDir, '../../react-refresh'),
        'App.jsx': target.dummyAppFile,
      },
    },
    externals: {
      react: 'React',
      'react-dom': 'ReactDOM',
    },
    output: {
      path: target.buildDir,
    },
    optimization: {
      runtimeChunk: 'single',
    },
    devtool: 'eval-source-map',
    devServer: {
      ...owWebpackConfig.devServer,
      publicPath: '/',
      contentBase: target.buildDir,
      stats: 'errors-only',
      port: 3005, // gets overridden below
    },
    plugins: [
      // DEFINE PLUGIN
      owWebpackConfig.plugins[0],
      new HtmlWebpackPlugin({
        title: `${target.fileName}`,
        template: path.resolve(__dirname, 'index.html'),
      }),
      new webpack.HotModuleReplacementPlugin(),
      new ReactRefreshWebpackPlugin(),
      new WebpackBar({ name: target.fileName }),
    ],
  };
  const webpackConfig = {
    ...owWebpackConfig,
    ...overrides,
  };
  const babelLoaderIndex = webpackConfig.module.rules.findIndex((l) => l.loader === 'babel-loader');
  if (!babelLoaderIndex) {
    console.error('Unable to find babel-loader');
    process.exit(1);
  }

  webpackConfig.module.rules = [
    ...webpackConfig.module.rules.slice(0, babelLoaderIndex),
    {
      ...webpackConfig.module.rules[babelLoaderIndex],
      exclude: (file) => file.match(webpackConfig.module.rules[babelLoaderIndex].exclude) && !file.endsWith('App.jsx'),
      options: {
        ...webpackConfig.module.rules[babelLoaderIndex].options,
        plugins: [require.resolve('react-refresh/babel')],
      },
    },
    ...webpackConfig.module.rules.slice(babelLoaderIndex + 1),
  ];

  delete webpackConfig.devServer.proxy;

  return webpackConfig;
}
