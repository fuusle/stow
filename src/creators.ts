import C from 'chalk';
import { dirExists } from './helpers';
import mkdirp from 'mkdirp';
import fs from 'fs';
import path from 'path';
import { Debugger } from 'debug';
import { JumpstartTarget } from './types';

export function createComponentStructure(target: JumpstartTarget, debug: Debugger) {
  console.log(C.cyan.bold(`✓ Creating component ${target.relativeFilePath}`));
  if (!dirExists(target.relativePath)) {
    console.log(C.cyan.dim(`  .. the path ${C.bold(target.relativePath)} did not exist, creating`));
    mkdirp.sync(target.relativePath);
  } else {
    debug(`Path ${target.relativePath} did exist`);
  }
  debug(`Creating COMPONENT-file "%s"`, target.relativePath);
  let componentContent = fs.readFileSync(path.resolve(__dirname, 'Component.jsx'), 'utf8');
  const camelCaseComponentName = `${target.componentName.toLowerCase()[0]}${target.componentName.slice(1)}`;
  componentContent = componentContent.replace(/COMPONENT_NAME/g, target.componentName);
  componentContent = componentContent.replace(/COMPONENT_CAMEL_CASE/g, camelCaseComponentName);
  fs.writeFileSync(target.relativeFilePath, componentContent, 'utf8');

  debug(`Creating STYLE-file "%s"`, target.styleFilePath);
  let styleFileContent = fs.readFileSync(path.resolve(__dirname, 'style.scss'), 'utf8');
  styleFileContent = styleFileContent.replace(/COMPONENT_CAMEL_CASE/g, camelCaseComponentName);
  fs.writeFileSync(target.styleFilePath, styleFileContent, 'utf8');

  debug(`Creating TEST-file "%s"`, target.styleFilePath);
  let testFileContent = fs.readFileSync(path.resolve(__dirname, 'Component.test.jsx'), 'utf8');
  testFileContent = testFileContent.replace(/COMPONENT_NAME/g, target.componentName);
  testFileContent = testFileContent.replace(/COMPONENT_PATH/g, `./${target.name}`);
  fs.writeFileSync(target.testFilePath, testFileContent, 'utf8');

  console.log(C.cyan.dim(`  .. done!`));
}

export function createJumpstartFile(target: JumpstartTarget, isCreated: boolean, debug: Debugger) {
  debug(
    'jumpstart-file "%s" for "%s" does not exists, creating "%s"',
    target.jumpstartFileName,
    target.relativeFilePath,
    target.jumpstartFilePath
  );
  let jumpstartFileContent = fs.readFileSync(
    path.resolve(__dirname, isCreated ? 'ComponentCreated.jumpstart.jsx' : 'Component.jumpstart.jsx'),
    'utf8'
  );
  jumpstartFileContent = jumpstartFileContent.replace(/COMPONENT_NAME/g, target.componentName);
  jumpstartFileContent = jumpstartFileContent.replace(/COMPONENT_PATH/g, `./${target.name}`);
  fs.writeFileSync(target.jumpstartFilePath, jumpstartFileContent, 'utf8');
}

export function createFakeAppEntryPoint(target: JumpstartTarget, debug: Debugger) {
  let entryPointAppFileContent = fs.readFileSync(path.resolve(__dirname, 'App.jsx'), 'utf8');
  entryPointAppFileContent = entryPointAppFileContent.replace(/COMPONENT_NAME/g, target.mountInAppTarget.componentName);
  entryPointAppFileContent = entryPointAppFileContent.replace(/COMPONENT_PATH/g, target.mountInAppTarget.filePath);
  debug('Creating build dir %s', target.buildDir);
  mkdirp.sync(target.buildDir);
  debug('Creating App entry point %s', target.dummyAppFile);
  fs.writeFileSync(target.dummyAppFile, entryPointAppFileContent, 'utf8');
}
