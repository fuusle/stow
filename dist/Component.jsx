import * as React from 'react';
import styles from './style.scss';

function COMPONENT_NAME() {
  return (
    <div className={styles.COMPONENT_CAMEL_CASE}>
      <p>Here you go -- a nice starting point!</p>
    </div>
  );
}

export default COMPONENT_NAME;
