"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createFakeAppEntryPoint = exports.createJumpstartFile = exports.createComponentStructure = void 0;
const chalk_1 = __importDefault(require("chalk"));
const helpers_1 = require("./helpers");
const mkdirp_1 = __importDefault(require("mkdirp"));
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
function createComponentStructure(target, debug) {
    console.log(chalk_1.default.cyan.bold(`✓ Creating component ${target.relativeFilePath}`));
    if (!helpers_1.dirExists(target.relativePath)) {
        console.log(chalk_1.default.cyan.dim(`  .. the path ${chalk_1.default.bold(target.relativePath)} did not exist, creating`));
        mkdirp_1.default.sync(target.relativePath);
    }
    else {
        debug(`Path ${target.relativePath} did exist`);
    }
    debug(`Creating COMPONENT-file "%s"`, target.relativePath);
    let componentContent = fs_1.default.readFileSync(path_1.default.resolve(__dirname, 'Component.jsx'), 'utf8');
    const camelCaseComponentName = `${target.componentName.toLowerCase()[0]}${target.componentName.slice(1)}`;
    componentContent = componentContent.replace(/COMPONENT_NAME/g, target.componentName);
    componentContent = componentContent.replace(/COMPONENT_CAMEL_CASE/g, camelCaseComponentName);
    fs_1.default.writeFileSync(target.relativeFilePath, componentContent, 'utf8');
    debug(`Creating STYLE-file "%s"`, target.styleFilePath);
    let styleFileContent = fs_1.default.readFileSync(path_1.default.resolve(__dirname, 'style.scss'), 'utf8');
    styleFileContent = styleFileContent.replace(/COMPONENT_CAMEL_CASE/g, camelCaseComponentName);
    fs_1.default.writeFileSync(target.styleFilePath, styleFileContent, 'utf8');
    debug(`Creating TEST-file "%s"`, target.styleFilePath);
    let testFileContent = fs_1.default.readFileSync(path_1.default.resolve(__dirname, 'Component.test.jsx'), 'utf8');
    testFileContent = testFileContent.replace(/COMPONENT_NAME/g, target.componentName);
    testFileContent = testFileContent.replace(/COMPONENT_PATH/g, `./${target.name}`);
    fs_1.default.writeFileSync(target.testFilePath, testFileContent, 'utf8');
    console.log(chalk_1.default.cyan.dim(`  .. done!`));
}
exports.createComponentStructure = createComponentStructure;
function createJumpstartFile(target, isCreated, debug) {
    debug('jumpstart-file "%s" for "%s" does not exists, creating "%s"', target.jumpstartFileName, target.relativeFilePath, target.jumpstartFilePath);
    let jumpstartFileContent = fs_1.default.readFileSync(path_1.default.resolve(__dirname, isCreated ? 'ComponentCreated.jumpstart.jsx' : 'Component.jumpstart.jsx'), 'utf8');
    jumpstartFileContent = jumpstartFileContent.replace(/COMPONENT_NAME/g, target.componentName);
    jumpstartFileContent = jumpstartFileContent.replace(/COMPONENT_PATH/g, `./${target.name}`);
    fs_1.default.writeFileSync(target.jumpstartFilePath, jumpstartFileContent, 'utf8');
}
exports.createJumpstartFile = createJumpstartFile;
function createFakeAppEntryPoint(target, debug) {
    let entryPointAppFileContent = fs_1.default.readFileSync(path_1.default.resolve(__dirname, 'App.jsx'), 'utf8');
    entryPointAppFileContent = entryPointAppFileContent.replace(/COMPONENT_NAME/g, target.mountInAppTarget.componentName);
    entryPointAppFileContent = entryPointAppFileContent.replace(/COMPONENT_PATH/g, target.mountInAppTarget.filePath);
    debug('Creating build dir %s', target.buildDir);
    mkdirp_1.default.sync(target.buildDir);
    debug('Creating App entry point %s', target.dummyAppFile);
    fs_1.default.writeFileSync(target.dummyAppFile, entryPointAppFileContent, 'utf8');
}
exports.createFakeAppEntryPoint = createFakeAppEntryPoint;
//# sourceMappingURL=creators.js.map