const Env = require('OMNIWEB_PATH/packages/nt-utils/src/Env');

Env.setup({
  NT_ENVDOMAIN: 'ENV_NAME',
  API_URL: 'https://ENV_PREFIX.norsk-tipping.no',
  SPORTSBOOK_URL: 'https://www-odv1.sport2.devqa.norsk-tipping.no',
  ADDITIONAL_ENV_VALUES
});

