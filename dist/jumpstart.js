#!/usr/bin/env node
"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chalk_1 = __importDefault(require("chalk"));
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const debug_1 = __importDefault(require("debug"));
const webpack_1 = __importDefault(require("webpack"));
const express_1 = __importDefault(require("express"));
const webpack_dev_middleware_1 = __importDefault(require("webpack-dev-middleware"));
const webpack_hot_middleware_1 = __importDefault(require("webpack-hot-middleware"));
const helpers_1 = require("./helpers");
const ChromeLauncher = __importStar(require("chrome-launcher"));
const get_port_1 = __importDefault(require("get-port"));
const types_1 = require("./types");
const creators_1 = require("./creators");
const debug = debug_1.default('jumpstart');
const [, , targetFile, ...flags] = process.argv;
const flagDescription = {
    [types_1.Flag.create]: ['Create the actual component'],
    [types_1.Flag.noLaunch]: ['Do not open in Chrome'],
    [types_1.Flag.noJumpstartFile]: ['Skip creation of jumpstart-file (e.g. MyComponent.jumpstart.jsx'],
    [types_1.Flag.temp]: ['Remove jumpstart-file when closing'],
    [types_1.Flag.help]: ['This message'],
};
const showHelp = () => {
    console.log(chalk_1.default.bold('jumpstart MyComponent.jsx[ --some-flag]'));
    console.log(chalk_1.default.gray('└─ .jsx, .js, .ts or .tsx file supported\n'));
    console.log(chalk_1.default.cyan(`Have Chrome Canary, but want to open in Chrome?`));
    console.log(chalk_1.default.cyan.dim('└─ BASH: export CHROME_PATH=/Applications/Google Chrome.app/Contents/MacOS/Google Chrome'));
    console.log(chalk_1.default.cyan.dim('└─ FISH: set -x CHROME_PATH /Applications/Google Chrome.app/Contents/MacOS/Google Chrome\n'));
    Object.entries(flagDescription).map(([flag, description]) => console.log(`${chalk_1.default.bold(flag)}: ${description}`));
    process.exit(0);
};
const isOn = (f) => flags.includes(f);
flags.forEach((flag) => {
    if (!(flag in flagDescription)) {
        console.log(chalk_1.default.red.bold(`unknown flag "${flag}"\n`));
        showHelp();
        process.exit(1);
    }
});
if (isOn(types_1.Flag.help)) {
    showHelp();
}
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Validate input
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if (!targetFile || !targetFile.match(/\.(jsx?|tsx?)$/)) {
    console.error(chalk_1.default.red.bold(!targetFile ? 'No input file given' : `Invalid file type given\n`));
    showHelp();
    process.exit(1);
}
if (!helpers_1.fileExists(targetFile) && !isOn(types_1.Flag.create)) {
    console.error(chalk_1.default.cyan.bold(`The component "${targetFile}" did not exist`));
    console.error(chalk_1.default.cyan.dim(`💡 you can create it by using the ${chalk_1.default.bold('--create')} option`));
    process.exit(1);
}
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// PATHS AND FILES RESOLVING
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const workingDir = process.cwd();
const packageJsonRoot = helpers_1.resolveClosestPackageJson(workingDir, ({ name }) => name === 'omniweb');
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const jumpstartNodeModulesDir = path_1.default.resolve(__dirname, '../', 'node_modules');
const jumpstartDir = path_1.default.resolve(__dirname);
const buildDir = path_1.default.join(__dirname, '../', '.build');
const target = {
    relativeFilePath: targetFile,
    relativePath: path_1.default.dirname(targetFile),
    filePath: path_1.default.resolve(workingDir, targetFile),
    path: path_1.default.dirname(path_1.default.resolve(workingDir, targetFile)),
    fileName: path_1.default.basename(targetFile),
    name: path_1.default.basename(targetFile, path_1.default.extname(targetFile)),
    extension: path_1.default.extname(targetFile),
    componentName: '',
    jumpstartFileName: '',
    jumpstartFilePath: '',
    styleFilePath: '',
    testFileName: '',
    testFilePath: '',
    checkSum: '',
    buildDir: '',
    globalCssFileName: '',
    globalCssFile: '',
    dummyAppFile: '',
    envSetupFile: '',
    mountInAppTarget: {
        componentName: '',
        filePath: '',
    },
};
target.componentName = target.name
    .split('.')
    .map((p) => `${p[0].toUpperCase()}${p.slice(1)}`)
    .join('');
target.jumpstartFileName = `${target.name}.jumpstart${target.extension}`;
target.jumpstartFilePath = path_1.default.resolve(target.path, target.jumpstartFileName);
target.styleFilePath = path_1.default.resolve(target.relativePath, 'style.scss');
target.testFileName = `${target.name}.test${target.extension}`;
target.testFilePath = path_1.default.resolve(target.path, target.testFileName);
target.checkSum = helpers_1.checkSumFromString(target.filePath);
target.buildDir = path_1.default.join(buildDir, target.checkSum);
target.globalCssFileName = 'main.css';
target.globalCssFile = path_1.default.join(buildDir, target.globalCssFileName);
target.dummyAppFile = path_1.default.join(target.buildDir, 'App.jsx');
target.envSetupFile = path_1.default.join(target.buildDir, 'setup.prod.js');
target.mountInAppTarget = {
    componentName: target.componentName,
    filePath: target.filePath,
};
const webpackClientConfig = path_1.default.resolve(packageJsonRoot.moduleDir, 'packages', 'app-web', 'webpack.dev.client.js');
// Create the component?
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if (isOn(types_1.Flag.create)) {
    if (!helpers_1.fileExists(target.jumpstartFilePath)) {
        creators_1.createComponentStructure(target, debug);
    }
    else {
        debug(`${types_1.Flag.create} was used, but the component already existed, skipping step`);
    }
}
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Should we use a jumpstart-file?
// ////////////////////////////////styleFilePath/////////////////////////////////////////////////////////////////////////////////////
const jumpstartFileDidAlreadyExists = helpers_1.fileExists(target.jumpstartFilePath);
if (!isOn(types_1.Flag.noJumpstartFile) && !targetFile.includes('.jumpstart.')) {
    debug('target file "%s" was NOT a jumpstart-file', targetFile);
    // create jumpstart-file if it doesn't exist
    if (!jumpstartFileDidAlreadyExists) {
        creators_1.createJumpstartFile(target, isOn(types_1.Flag.create), debug);
    }
    else {
        debug('jumpstart-file (%s) already exists', target.jumpstartFileName, targetFile);
    }
    // mount the jumpstart file in the dummy-app instead of the component
    target.mountInAppTarget.componentName = `${target.mountInAppTarget.componentName}Jumpstart`;
    target.mountInAppTarget.filePath = target.jumpstartFilePath;
    debug('mount jumpstart-file in fake app %o', target.mountInAppTarget);
}
else {
    debug('target file "%s" was a jumpstart-file, using it directly...', targetFile);
}
if (isOn(types_1.Flag.temp)) {
    if (!jumpstartFileDidAlreadyExists) {
        debug(`${types_1.Flag.temp} flag raised, defering cleanup on exit`);
        process.stdin.resume();
        let isAlreadyCleand = false;
        function onProgramExit() {
            if (isAlreadyCleand)
                return;
            debug(`program is exiting and the ${types_1.Flag.temp} flag raised, cleaning now`);
            debug(`removing ${target.jumpstartFilePath}`);
            try {
                fs_1.default.unlinkSync(target.jumpstartFilePath);
                isAlreadyCleand = true;
            }
            catch (e) {
                console.error(e);
            }
            process.exit();
        }
        //do something when app is closing
        process.on('exit', onProgramExit);
        //catches ctrl+c event
        process.on('SIGINT', onProgramExit);
        // catches "kill pid" (for example: nodemon restart)
        process.on('SIGUSR1', onProgramExit);
        process.on('SIGUSR2', onProgramExit);
        //catches uncaught exceptions
        process.on('uncaughtException', onProgramExit);
    }
    else {
        debug(`${types_1.Flag.temp} flag raised, but the jumpstart-file already existed (will not clean)`);
    }
}
// PREPARE THE ENTRY POINT
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
creators_1.createFakeAppEntryPoint(target, debug);
fs_1.default.copyFileSync(path_1.default.resolve(jumpstartDir, './setup.prod.js'), target.envSetupFile);
// WEBPACK IT!
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const webpackConfig = helpers_1.reconfigureOmniWebWebpackConfiguration(target, webpackClientConfig, jumpstartNodeModulesDir, workingDir, debug);
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// COMMON STYLE
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if (!helpers_1.fileExists(target.globalCssFile)) {
    helpers_1.webpackBuildGlobalOmniWebCss(target, packageJsonRoot.moduleDir, debug);
}
else {
    debug('already have a global css-file from nt-css-rejumpstart (%s)', target.globalCssFile);
}
// Webpack needs to resolve .babel stuff from actual file location
process.chdir(workingDir);
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SERVE IT!
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
(() => __awaiter(void 0, void 0, void 0, function* () {
    const port = yield get_port_1.default({ port: [...Array(20)].map((_, i) => 3005 + i) });
    const startingUrl = `http://localhost:${port}`;
    console.log(chalk_1.default.cyan(`✓ Preparing serving of ${target.fileName}`));
    console.log(chalk_1.default.cyan.dim(`  .. make global CSS available under "/build"`));
    console.log(chalk_1.default.cyan.dim(`  .. make OmniWeb-assets available under "/ow/assets"`));
    console.log('');
    if (!isOn(types_1.Flag.noLaunch)) {
        console.log(chalk_1.default.cyan.bold(`✓ Auto-launching Chrome when ready (${startingUrl})`));
    }
    else {
        console.log(chalk_1.default.cyan.bold(`✓ Open your browser at ${startingUrl}`));
    }
    console.log(chalk_1.default.cyan.dim(`  └─ ${target.filePath}`));
    if (!isOn(types_1.Flag.noJumpstartFile)) {
        console.log(chalk_1.default.cyan.dim(`  └─ ${target.jumpstartFilePath}`));
    }
    console.log(chalk_1.default.cyan.dim(`  └─ ${target.testFilePath}`));
    console.log(chalk_1.default.cyan.dim(`  └─ ${target.styleFilePath}`));
    console.log(chalk_1.default.cyan.dim(`  └─ ${target.dummyAppFile}`));
    const app = express_1.default();
    const compiler = webpack_1.default(webpackConfig);
    // Tell express to use the webpack-dev-middleware and use the webpack.config.js
    // configuration file as a base.
    app.use(webpack_dev_middleware_1.default(compiler, {
        publicPath: webpackConfig.output.publicPath,
        stats: 'errors-only',
        logLevel: 'silent',
    }));
    app.use('/assets', express_1.default.static(path_1.default.resolve(__dirname, 'static')));
    app.use('/build', express_1.default.static(buildDir));
    app.use('/ow/assets', express_1.default.static(path_1.default.resolve(packageJsonRoot.moduleDir, 'packages/app-web/src/public/ow/assets')));
    app.use(webpack_hot_middleware_1.default(compiler, {
        log: false,
        path: '/__webpack_hmr',
        heartbeat: 2000,
    }));
    webpackConfig.devServer.port = port;
    app.listen(port, function () {
        const chromeFlags = [
            '--auto-open-devtools-for-tabs',
            ...ChromeLauncher.Launcher.defaultFlags().filter((flag) => !['--disable-extensions', '--disable-extensions'].includes(flag)),
        ];
        if (!isOn(types_1.Flag.noLaunch)) {
            ChromeLauncher.launch({
                startingUrl,
                userDataDir: false,
                ignoreDefaultFlags: true,
                chromeFlags,
            }).catch((err) => {
                debug('Got error from Chrome launcher', err);
            });
        }
    });
}))();
// [ ] create Component (folder, Component, test and jumpstart-file)
// [ ] run test?
// [ ] doc → debugger;
// [ ] start with prod config
// [ ] Env.setup() → make it easy to edit though
//     [ ] dynamically use Env from current OW-installation
//     [ ] make it editable vs --env-prod --env-dev
// [ ] Persist API-snapshot, use in tests??
//# sourceMappingURL=jumpstart.js.map