import * as React from 'react';
import * as ReactDom from 'react-dom';
import COMPONENT_NAME from 'COMPONENT_PATH';
import './setup.prod';

const rootNode = document.getElementById('root');

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(error, errorInfo) {
    console.log(error, errorInfo);
  }

  render() {
    if (this.state.hasError) {
      return <h1>Something went wrong.</h1>;
    }
    return this.props.children;
  }
}

ReactDom.render((
  <ErrorBoundary>
    <COMPONENT_NAME/>
  </ErrorBoundary>
), rootNode);
