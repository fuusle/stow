"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Flag = void 0;
var Flag;
(function (Flag) {
    Flag["create"] = "--create";
    Flag["noLaunch"] = "--no-launch";
    Flag["noJumpstartFile"] = "--no-jumpstart-file";
    Flag["temp"] = "--temp";
    Flag["help"] = "--help";
})(Flag = exports.Flag || (exports.Flag = {}));
//# sourceMappingURL=types.js.map