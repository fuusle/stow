"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.reconfigureOmniWebWebpackConfiguration = exports.webpackBuildGlobalOmniWebCss = exports.checkSumFromString = exports.resolveClosestPackageJson = exports.dirExists = exports.fileExists = void 0;
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const crypto_1 = __importDefault(require("crypto"));
const chalk_1 = __importDefault(require("chalk"));
const webpack_1 = __importDefault(require("webpack"));
const mini_css_extract_plugin_1 = __importDefault(require("mini-css-extract-plugin"));
const html_webpack_plugin_1 = __importDefault(require("html-webpack-plugin"));
const react_refresh_webpack_plugin_1 = __importDefault(require("@pmmmwh/react-refresh-webpack-plugin"));
const webpackbar_1 = __importDefault(require("webpackbar"));
function fileExists(file) {
    return fs_1.default.existsSync(file);
}
exports.fileExists = fileExists;
function dirExists(path) {
    return fs_1.default.existsSync(path);
}
exports.dirExists = dirExists;
function resolveClosestPackageJson(cwd, peek) {
    const pathLevels = cwd.split(path_1.default.sep);
    const peekHelper = (file) => {
        const content = JSON.parse(fs_1.default.readFileSync(file, 'utf-8'));
        return !peek || peek(content);
    };
    for (let m = pathLevels.length; m > 0; m--) {
        const pathToLookIn = pathLevels.slice(0, m).join(path_1.default.sep);
        const packageJson = path_1.default.resolve(pathToLookIn, 'package.json');
        if (packageJson && fileExists(packageJson) && (!peek || peekHelper(packageJson))) {
            return { moduleDir: pathToLookIn, relativePathToTarget: pathLevels.slice(m).join(path_1.default.sep) };
        }
    }
    throw Error('Unable to find package.json');
}
exports.resolveClosestPackageJson = resolveClosestPackageJson;
function checkSumFromString(str) {
    return crypto_1.default.createHash('sha256').update(str, 'utf8').digest('hex');
}
exports.checkSumFromString = checkSumFromString;
function webpackBuildGlobalOmniWebCss(target, omniwebRootPath, debug) {
    console.log(chalk_1.default.cyan.bold(`✓ Building CSS reboot/reset from nt-css-reboot`));
    webpack_1.default({
        entry: path_1.default.resolve(omniwebRootPath, 'packages/nt-css-reboot/src/styles/main.scss'),
        output: {
            path: path_1.default.resolve(__dirname, '../.build'),
        },
        module: {
            rules: [
                {
                    exclude: /node_modules/,
                    test: /\.scss$/,
                    use: [
                        mini_css_extract_plugin_1.default.loader,
                        {
                            loader: 'css-loader',
                        },
                        {
                            loader: 'postcss-loader',
                        },
                        {
                            loader: 'sass-loader',
                        },
                    ],
                },
            ],
        },
        plugins: [
            new mini_css_extract_plugin_1.default({
                filename: target.globalCssFileName,
            }),
        ],
    }, (err, stats) => {
        // Stats Object
        if (err || stats.hasErrors()) {
            console.error('Error found generating global css');
            console.error(err);
            return;
        }
        console.log(chalk_1.default.cyan.dim(`  .. done!`));
        debug('successfully generated global css from OmniWeb/nt-css-rejumpstart (%s)', target.globalCssFile);
    });
}
exports.webpackBuildGlobalOmniWebCss = webpackBuildGlobalOmniWebCss;
function reconfigureOmniWebWebpackConfiguration(target, webpackClientConfig, jumpstartNodeModulesDir, workingDir, debug) {
    const owWebpackConfig = require(webpackClientConfig);
    const overrides = Object.assign(Object.assign({}, owWebpackConfig), { context: workingDir, entry: ['webpack-hot-middleware/client', target.dummyAppFile], resolve: Object.assign(Object.assign({}, owWebpackConfig.resolve), { alias: {
                'react-refresh': dirExists(path_1.default.resolve(jumpstartNodeModulesDir, 'react-refresh'))
                    ? path_1.default.resolve(jumpstartNodeModulesDir, 'react-refresh')
                    : path_1.default.resolve(jumpstartNodeModulesDir, '../../react-refresh'),
                'App.jsx': target.dummyAppFile,
            } }), externals: {
            react: 'React',
            'react-dom': 'ReactDOM',
        }, output: {
            path: target.buildDir,
        }, optimization: {
            runtimeChunk: 'single',
        }, devtool: 'eval-source-map', devServer: Object.assign(Object.assign({}, owWebpackConfig.devServer), { publicPath: '/', contentBase: target.buildDir, stats: 'errors-only', port: 3005 }), plugins: [
            // DEFINE PLUGIN
            owWebpackConfig.plugins[0],
            new html_webpack_plugin_1.default({
                title: `${target.fileName}`,
                template: path_1.default.resolve(__dirname, 'index.html'),
            }),
            new webpack_1.default.HotModuleReplacementPlugin(),
            new react_refresh_webpack_plugin_1.default(),
            new webpackbar_1.default({ name: target.fileName }),
        ] });
    const webpackConfig = Object.assign(Object.assign({}, owWebpackConfig), overrides);
    const babelLoaderIndex = webpackConfig.module.rules.findIndex((l) => l.loader === 'babel-loader');
    if (!babelLoaderIndex) {
        console.error('Unable to find babel-loader');
        process.exit(1);
    }
    webpackConfig.module.rules = [
        ...webpackConfig.module.rules.slice(0, babelLoaderIndex),
        Object.assign(Object.assign({}, webpackConfig.module.rules[babelLoaderIndex]), { exclude: (file) => file.match(webpackConfig.module.rules[babelLoaderIndex].exclude) && !file.endsWith('App.jsx'), options: Object.assign(Object.assign({}, webpackConfig.module.rules[babelLoaderIndex].options), { plugins: [require.resolve('react-refresh/babel')] }) }),
        ...webpackConfig.module.rules.slice(babelLoaderIndex + 1),
    ];
    delete webpackConfig.devServer.proxy;
    return webpackConfig;
}
exports.reconfigureOmniWebWebpackConfiguration = reconfigureOmniWebWebpackConfiguration;
//# sourceMappingURL=helpers.js.map