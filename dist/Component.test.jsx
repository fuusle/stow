import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import COMPONENT_NAME from 'COMPONENT_PATH';

describe('COMPONENT_NAME', () => {
  it('should render without crashing', () => {
    expect(() => shallow(<COMPONENT_NAME />)).to.not.throw();
  });
});
