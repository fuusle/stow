const Env = require('/Users/arild/workspace/nt/omniweb/packages/nt-utils/src/Env');

Env.setup({
  NT_ENVDOMAIN: 'dev',
  API_URL: 'https://api.norsk-tipping.no',
  LOGIN_URL: 'https://api.norsk-tipping.no',
  TRACKING_ENV: 'dev',
  FAKE_API_URL: 'https://localhost:3003',
  FEATURE_ENV: 'dev',
  MOBILE_URL: 'https://www.norsk-tipping.no/mobile-internet',
  WEB_URL: 'https://www.norsk-tipping.no',
  OMNIWEB_URL: 'https://www.norsk-tipping.no',
  CONTENT_API_URL: 'https://www.norsk-tipping.no',
  GTM_TRACKING_ID: 'dev',
  SPORTSBOOK_URL: 'https://www-odv1.sport2.devqa.norsk-tipping.no',
});

